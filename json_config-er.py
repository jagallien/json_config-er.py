#!/usr/bin/python3

from datetime import date # Used for naming our file w/ the current date.
import orionsdk
import urllib3
import netmiko
import paramiko
import json
import sys

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning) #removes "insecure" warnings from CLI output :\

today = (date.today().strftime('%Y%m%d'))
output_file = open((today + 'json_config-er_v1_failures.csv'), mode='w')
connector = orionsdk.SwisClient(sys.argv[1], sys.argv[2], sys.argv[3])
json_output = open(('solarwinds_objects_' + today + '.json'), mode='w')

def change_maker(ip,hostname):
    global output_file
    try:
        sshto = netmiko.ConnectHandler(ip=ip, device_type='cisco_ios', username=sys.argv[2], password=sys.argv[3])
        sshto.enable(cmd='enable') #This puts us in enable mode. More stable than sending the enable command.
        config_commands = [ 'command 1',
            'command 2']
        print(sshto.send_config_set(config_commands)) #enacts the commands with config terminal mode.
        sshto.disconnect()
    except (paramiko.ssh_exception.NoValidConnectionsError,
            netmiko.ssh_exception.NetMikoTimeoutException,
            paramiko.ssh_exception.AuthenticationException,
            paramiko.ssh_exception.SSHException):
        output_file.write(ip + ',' + hostname)

query = "SELECT SysName, NodeID, CoreNodeID, AgentIP, Uri, StatusText, SNMPLevel, Community, CommunityReadWrite FROM Cirrus.Nodes WHERE SNMPLevel='3'"
results = connector.query(query)

json_output.write(json.dumps(results))

with open(('solarwinds_objects_' + today + '.json')) as f:
    devices = f.read()
    dict = json.loads(devices)


for i in dict['results']:
    print('Changing Device: {0} -- Removing SNMP Community Strings...\n' .format(i['SysName']))
    change_maker(i['AgentIP'],i['SysName'])
